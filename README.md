Synopsis
---
"CBOR Serialze" is a library for Haskell which allows you to serialize you data types into CBOR format to pass on the network or store at the config file. CBOR format is more compact and is faster to parse by a machine then text formats like JSON or XML. On the other hand it's much less human-readable.

===

Code Example
---
Say you have a data type ControlMessage:

<pre>
data ControlMessage = ControlMessage {sequence_id::Int, packet_id::Int, text_content::Data.Text.Text}
</pre>

To serialize it into CBOR you should do this:
<pre>
instance Serializeable ControlMessage where
    cbor (ControlMessage sid pid tc) = CBOR_Map [
                                                        (cw 1, cbor sid)
                                                    ,   (cw 2, cbor pid)
                                                    ,   (cw 3, cbor tc)
                                                ]
    uncbor x = ControlMessage  (x//1) (x//2) (x//3)
</pre>

Now you can simply serialize your ControlMessage and send over the network. On the other end you can deserialize it:
<pre>
control_message = ControlMessage 1 1 (Data.Text.pack "PING")
byte_string = Data.CBOR.Serialize.serialize control_message
{- Send byte_string over the network -}
{- ... -}
{- Receive byte_string from the network -}
msg_received = Data.CBOR.Serialize.deserialize byte_string :: ControlMessage
</pre>

===

Motivation
---
I want to have the opportunity to serialize data types to compact and fast-parsed binary format, because text formats are way too redundant.

===

Installation
---
git clone https://bitbucket.org/gvfnix/cbor-serialize.git
cd cbor-serialize
cabal configure --enable-tests
cabal build
cabal test
cabal installl

===

API Reference
---
See Code Example

===

Tests
---
To run tests, get it from BitBucket, then run "cabal test".

===

Contributors
---
https://bitbucket.org/gvfnix/cbor-serialize

===

License
---
BSD3