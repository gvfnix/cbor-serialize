module Data.CBOR.Serialize (
        Serializable, serialize, deserialize, cbor, uncbor, cw, ucw,
        ct, uct, (///), (//), (/-)
    ) where

import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as LBS
import qualified Data.ByteString.UTF8 as BSU
import Data.Int (Int8, Int16, Int32, Int64)
import Data.Word (Word8, Word16, Word32, Word64)
import Data.Maybe(Maybe(..), fromJust)
import qualified Data.Text as T
import qualified Data.Char as C
import Data.CBOR
import Data.Binary.CBOR
import Data.Binary.Put(runPut)
import Data.Binary.Get(runGet)

class Serializable a where
    cbor :: a -> CBOR
    uncbor :: CBOR -> a

instance Serializable Bool where
    cbor True = CBOR_True
    cbor False = CBOR_False
    uncbor CBOR_True = True
    uncbor CBOR_False = False
    uncbor _ = error "Must be CBOR_True or CBOR_False"

instance Serializable Int where
    cbor x = if x < 0 then CBOR_SInt f else CBOR_UInt f where f = fromIntegral x
    uncbor (CBOR_SInt x) = fromInteger x
    uncbor (CBOR_UInt x) = fromInteger x
    uncbor _ = error "Must be CBOR_UInt or CBOR_SInt"

instance Serializable Int8 where
    cbor x = if x < 0 then CBOR_SInt f else CBOR_UInt f where f = fromIntegral x
    uncbor (CBOR_SInt x) = fromInteger x
    uncbor (CBOR_UInt x) = fromInteger x
    uncbor _ = error "Must be CBOR_UInt or CBOR_SInt"

instance Serializable Int16 where
    cbor x = if x < 0 then CBOR_SInt f else CBOR_UInt f where f = fromIntegral x
    uncbor (CBOR_SInt x) = fromInteger x
    uncbor (CBOR_UInt x) = fromInteger x
    uncbor _ = error "Must be CBOR_UInt or CBOR_SInt"

instance Serializable Int32 where
    cbor x = if x < 0 then CBOR_SInt f else CBOR_UInt f where f = fromIntegral x
    uncbor (CBOR_SInt x) = fromInteger x
    uncbor (CBOR_UInt x) = fromInteger x
    uncbor _ = error "Must be CBOR_UInt or CBOR_SInt"

instance Serializable Int64 where
    cbor x = if x < 0 then CBOR_SInt f else CBOR_UInt f where f = fromIntegral x
    uncbor (CBOR_SInt x) = fromInteger x
    uncbor (CBOR_UInt x) = fromInteger x
    uncbor _ = error "Must be CBOR_UInt or CBOR_SInt"

instance Serializable Word where
    cbor x = CBOR_UInt f where f = fromIntegral x
    uncbor (CBOR_UInt x) = fromInteger x
    uncbor _ = error "Must be CBOR_UInt"

instance Serializable Word8 where
    cbor x = CBOR_Byte x
    uncbor (CBOR_Byte x) = x
    uncbor _ = error "Must be CBOR_Byte"

instance Serializable Word16 where
    cbor x = CBOR_UInt f where f = fromIntegral x
    uncbor (CBOR_UInt x) = fromInteger x
    uncbor _ = error "Must be CBOR_UInt"

instance Serializable Word32 where
    cbor x = CBOR_UInt f where f = fromIntegral x
    uncbor (CBOR_UInt x) = fromInteger x
    uncbor _ = error "Must be CBOR_UInt"

instance Serializable Word64 where
    cbor x = CBOR_UInt f where f = fromIntegral x
    uncbor (CBOR_UInt x) = fromInteger x
    uncbor _ = error "Must be CBOR_UInt"

instance Serializable Float where
    cbor x = CBOR_Float x
    uncbor (CBOR_Float x) = x
    uncbor _ = error "Must be CBOR_Float"

instance Serializable Double where
    cbor x = CBOR_Double x
    uncbor (CBOR_Double x) = x
    uncbor _ = error "Must be CBOR_Double"

instance Serializable BS.ByteString where
    cbor x = CBOR_BS x
    uncbor (CBOR_BS x) = x
    uncbor _ = error "Must be CBOR_BS"

instance Serializable LBS.ByteString where
    cbor x = CBOR_BS s where s = LBS.toStrict x
    uncbor (CBOR_BS x) = l where l = LBS.fromStrict x
    uncbor _ = error "Must be CBOR_BS"

instance Serializable T.Text where
    cbor x = CBOR_TS b where b = BSU.fromString s
                             s = T.unpack x
    uncbor (CBOR_TS x) = t where t = T.pack s
                                 s = BSU.toString x
    uncbor _ = error "Must be CBOR_TS"

instance Serializable Char where
    cbor x = CBOR_UInt f where f = fromIntegral o
                               o = C.ord x
    uncbor (CBOR_UInt x) = C.chr i where i = fromInteger x
    uncbor _ = error "Must be CBOR_UInt"

instance (Serializable a) => Serializable [a] where
    cbor x = CBOR_Array m where m = map cbor x
    uncbor (CBOR_Array x) = m where m = map uncbor x
    uncbor _ = error "Must be CBOR_Array"

instance (Serializable a) => Serializable (Maybe a) where
    cbor (Just x) = cbor x
    cbor Nothing = CBOR_NULL
    uncbor CBOR_NULL = Nothing
    uncbor x = Just $ uncbor x

instance Serializable CBOR where
    cbor x = x
    uncbor x = x

serialize :: Serializable a => a -> LBS.ByteString
serialize = runPut . putCBOR . cbor

deserialize :: Serializable a => LBS.ByteString -> a
deserialize x = uncbor $ runGet getCBOR x

(///) :: (Serializable a, Serializable b) => CBOR -> a -> b
(///) (CBOR_Map x) k = u where
    u = uncbor j
    j = fromJust l
    l = lookup c x
    c = cbor k

(//) :: (Serializable a) => CBOR -> Word16 -> a
(//) = (///)

(/-) :: (Serializable a) => CBOR -> T.Text -> a
(/-) = (///)

cw :: Word16 -> CBOR
cw = cbor

ucw :: CBOR -> Word16
ucw = uncbor

ct :: T.Text -> CBOR
ct = cbor

uct :: CBOR -> T.Text
uct = uncbor