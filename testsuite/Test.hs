import Test.HUnit
import Data.Int
import Data.Word
import Data.Maybe
import Data.ByteString
import Data.ByteString.Lazy
import Data.Text
import Data.CBOR
import Data.CBOR.Serialize
import System.Exit

data RecordType1 = RecordType1 {
        field11::Maybe Int
    ,   field12::Bool
    ,   field13::Float
    ,   field14::Text
    ,   field15::[Double]
} deriving (Eq, Show)

data RecordType2 = RecordType2 {
        field21::Int
    ,   field22::Data.ByteString.Lazy.ByteString
    ,   field23::RecordType1
} deriving (Eq, Show)


instance Serializable RecordType1 where
    cbor x = CBOR_Map [
                            (cw 1, cbor $ field11 x)
                        ,   (cw 2, cbor $ field12 x)
                        ,   (cw 3, cbor $ field13 x)
                        ,   (cw 4, cbor $ field14 x)
                        ,   (cw 5, cbor $ field15 x)
                      ]
    uncbor x = RecordType1 (x//1) (x//2) (x//3) (x//4) (x//5)

instance Serializable RecordType2 where
    cbor x = CBOR_Map [
                            (cw 1, cbor $ field21 x)
                        ,   (cw 2, cbor $ field22 x)
                        ,   (cw 3, cbor $ field23 x)
                      ]
    uncbor x = RecordType2 (x//1) (x//2) (x//3)


varRecord1 = RecordType1 (Just 2) (False) (9.82) (Data.Text.pack "Interpretation") ([2.41, 2.23252, 4.23526663])
varRecord2 = RecordType2 (19) (Data.ByteString.Lazy.pack [121, 19, 221]) (varRecord1)
varNothing = Nothing
varTrue = True
varFalse = False
varIntp = 1 :: Int
varInt8p = 8 :: Int8
varInt16p = 16 :: Int16
varInt32p = 32 :: Int32
varInt64p = 64 :: Int64
varIntn = (-1) :: Int
varInt8n = (-8) :: Int8
varInt16n = (-16) :: Int16
varInt32n = (-32) :: Int32
varInt64n = (-64) :: Int64
varWord8 = 1 :: Word8
varWord16 = 16 :: Word16
varWord32 = 32 :: Word32
varWord64 = 64 :: Word64
varFloat = 1.23 :: Float
varDouble = 9.2234252342 :: Double
varText = Data.Text.pack "Serialize"
varBS = Data.ByteString.pack [1,2,3]
varLBS = Data.ByteString.Lazy.pack [1,2,3]
varArray = [1..9] :: [Int]
varChar = 'S'

convert :: (Serializable a, Serializable a1) => a1 -> a
convert = deserialize . serialize 

testFalse =         TestCase $ assertEqual "False converts to False"
                                (convert False)
                                (False)
testTrue =          TestCase $ assertEqual "True converts to True"
                                (convert True)
                                (True)
testIntp =          TestCase $ assertEqual "Positive Int converts to Int"
                                (convert varIntp)
                                (varIntp)
testIntn =          TestCase $ assertEqual "Negative Int converts to Int"
                                (convert varIntn)
                                (varIntn)
testInt8p =         TestCase $ assertEqual "Positive Int8 converts to Int"
                                (convert varInt8p)
                                (varInt8p)
testInt8n =         TestCase $ assertEqual "Negative Int8 converts to Int"
                                (convert varInt8n)
                                (varInt8n)
testInt16p =        TestCase $ assertEqual "Positive Int16 converts to Int"
                                (convert varInt16p)
                                (varInt16p)
testInt16n =        TestCase $ assertEqual "Negative Int16 converts to Int"
                                (convert varInt16n)
                                (varInt16n)
testInt32p =        TestCase $ assertEqual "Positive Int32 converts to Int"
                                (convert varInt32p)
                                (varInt32p)
testInt32n =        TestCase $ assertEqual "Negative Int32 converts to Int"
                                (convert varInt32n)
                                (varInt32n)
testInt64p =        TestCase $ assertEqual "Positive Int64 converts to Int"
                                (convert varInt64p)
                                (varInt64p)
testInt64n =        TestCase $ assertEqual "Negative Int64 converts to Int"
                                (convert varInt64n)
                                (varInt64n)
testWord8 =         TestCase $ assertEqual "Word8 converts to Word8"
                                (convert varWord8)
                                (varWord8)
testWord16 =        TestCase $ assertEqual "Word16 converts to Word16"
                                (convert varWord16)
                                (varWord16)
testWord32 =        TestCase $ assertEqual "Word32 converts to Word32"
                                (convert varWord32)
                                (varWord32)
testWord64 =        TestCase $ assertEqual "Word64 converts to Word64"
                                (convert varWord64)
                                (varWord64)
testFloat =         TestCase $ assertEqual "Float converts to Float"
                                (convert varFloat)
                                (varFloat)
testDouble =         TestCase $ assertEqual "Double converts to Double"
                                (convert varDouble)
                                (varDouble)
testChar    =       TestCase $ assertEqual "Char converts to Char"
                                (convert varChar)
                                (varChar)
testArray    =       TestCase $ assertEqual "Array converts to Array"
                                (convert varArray)
                                (varArray)
testNothing =       TestCase $ assertEqual "Nothing converts to Nothing"
                                (convert (Nothing :: Maybe Bool))
                                (Nothing :: Maybe Bool)
testRecord =        TestCase $ assertEqual "Record converts to Record"
                                (convert varRecord2)
                                (varRecord2)

tests = TestList    [
                            testFalse
                        ,   testTrue
                        ,   testIntp
                        ,   testIntn
                        ,   testInt8p
                        ,   testInt8n
                        ,   testInt16p
                        ,   testInt16n
                        ,   testInt32p
                        ,   testInt32n
                        ,   testInt64p
                        ,   testInt64n
                        ,   testWord8
                        ,   testWord16
                        ,   testWord32
                        ,   testWord64
                        ,   testFloat
                        ,   testDouble
                        ,   testChar
                        ,   testArray
                        ,   testNothing
                        ,   testRecord
                    ]


main = do
    c <- runTestTT tests
    let summary = (errors c) + (failures c)
    if summary == 0 then return ExitSuccess
                    else return (ExitFailure 1)